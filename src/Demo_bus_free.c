#include "headers/twi_driver.h"
#include "headers/uart_driver.h"
#include "headers/timer0_driver.h"
#include "headers/timer2_driver.h"
#include "headers/common_definitions.h"
#include "headers/ui_driver.h"
#include <avr/sleep.h>

//D9 D7 push button

// Timer 2
//D3  -green  D11 - red(Acc) led
#define LSBgyro 16.38
// #define LSBgyro 65.54
#define sample_period 0.024

//If one wants the angle to return to 0 at 360
// #define __360_OFFSET

#define Alimit1 50
#define Alimit2 250
#define Alimit3 500
#define Alimit4 1000
// #define Alimit1 0
// #define Alimit2 0
// #define Alimit3 0
// #define Alimit4 0

#define Glimit1 1
#define Glimit2 3
#define Glimit3 7
#define Glimit4 12

int main (void) {
        
    //Initiate LED connected to digital pin 13
        initLED();
    //Initiate UART for communication with PC and Debugging purposes
        initUART();
    //Initiate TWI and free the I2C bus
        initTWI();
    //Enable global interrupt
        sei();

    //Welcome
        completePrint("\nInitiating! ");
        char u[15];
        intToStr(123-456789,u);
        completePrint("Cool\n");
        completePrint(u);

    //Setup the sensor module
        // Activate the sensor module by selecting the clock
        uint8_t writeData[] = {0};
        twiMaster(0b01101000,twi_write,0x6B,writeData,1); 
        while (!twiReady());
        // Configure DLPF 
        writeData[0] = 6;
        twiMaster(0b01101000,twi_write,0x1A,writeData,1); 
        while (!twiReady());
        // Configure Gyro max = 500deg/s = 1, 1000d/s = 2, 2000d/s = 3
        writeData[0] = 0x18;
        twiMaster(0b01101000,twi_write,0x1B,writeData,1); 
        while (!twiReady());

    //Initialize required variables
        uint8_t readAccZ[2];
        uint8_t readGyrY[2];
        float Yangle = 0;
        volatile int16_t Az,Gy,Az_once,Gy_once,Az_snapshot = 0;
        uint8_t once = true;
        uint16_t twi_cnt = 0;
        uint16_t print_cnt = 0;
        uint16_t userActivity_cnt = 1;
        uint8_t userSensitivity = 1;
        uint8_t userSnapshot_buffer = 0;
        uint16_t userHoldingButton_upcnt = 0;
        uint16_t userHoldingButton_downcnt = 255;

    while(1){
            if (twiReady()) {
                twiMaster(0b1101000,twi_read,0x3F,readAccZ,2);
            }       
    }
}
