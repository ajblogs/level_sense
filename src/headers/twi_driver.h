//Refer ATmega48A/PA/88A/PA/168A/PA/328/P megaAVR ® Data Sheet 
//Section 22 for more information

#ifndef HEADERS_TWI_DRIVER_H
#define HEADERS_TWI_DRIVER_H

#include <avr/io.h>
#include <util/twi.h>
#include <avr/interrupt.h>
#include "inbuilt_led_driver.h"

#define TWI_INT 1<<7
#define TWI_EN_ACK 1<<6
#define TWI_START 1<<5
#define TWI_STOP 1<<4
#define TWI_COL 1<<3
#define TWI_EN 1<<2
#define TWI_EN_INT 1

#define TWI_SEND TWI_INT | TWI_EN
#define TWI_NEXT TWI_INT | TWI_EN
#define TWI_RECV TWI_INT | TWI_EN
#define TWI_SEND_START TWI_SEND | TWI_START 
#define TWI_SEND_STOP TWI_SEND | TWI_STOP 

// #define CPUfreq 16000000
// #define twiBitRate(SCLfreq) ((CPUfreq/SCLfreq)-16)/2 
#define twiBitRate 72; //for SCL = 100KHz

#define SDA 1<<4
#define SCL 1<<5
#define SDA_OUT() DDRC |= SDA
#define SDA_LOW() PORTC &= ~(SDA)
#define SDA_HIGH() PORTC |= SDA

#define SCL_OUT() DDRC |= SCL
#define SCL_LOW() PORTC &= ~(SCL)
#define SCL_HIGH() PORTC |= SCL
#define SDA_FREE() DDRC &= ~(SDA); PORTC &= ~(SDA)
#define SDA_IN ((PINC>>4) & 1)

#define DELAY(val) for (uint64_t i=0; i<val; i++);

void twiResetBus(){
    SCL_OUT();
    for (int i = 0; i<10; i++) {
        SDA_FREE();
        SCL_LOW();   
        DELAY(30);
        if (SDA_IN == 0) {
            SCL_HIGH();
            DELAY(60);
        } else {
            //Send stop
            SDA_OUT();
            DELAY(5);
            SDA_LOW();
            DELAY(10);
            SCL_HIGH();
            DELAY(15);
            SDA_HIGH();
            DELAY(30);
        }
    }
}
//Initialize SDA 4, SCL 5
//set SCL frequency
#define initTWI() \
    twiResetBus(); \
    DDRC &= ~(1<<5|1<<4); \
    PORTC |= 1<<5|1<<4; \
    TWBR = twiBitRate; \
    sei();

#define true 1
#define false 0

#define twi_read 11
#define twi_write 10

#define ok 0
#define busy 1
#define avail 2
#define error 3
#define done avail

volatile uint8_t _twiCnt = 0;
#define set_twiCnt(val) _twiCnt = val
#define get_twiCnt _twiCnt

volatile uint8_t _twiSla = 0;
#define set_twiSla(val) _twiSla = val
#define get_twiSla _twiSla << 1

volatile uint8_t _twiRW = 0;
#define get_twiRW _twiRW
#define set_twiRW(val) _twiRW = val 

volatile uint8_t _twiRegAddr = 0;
#define set_twiRegAddr(val) _twiRegAddr = val 
#define get_twiRegAddr _twiRegAddr

volatile uint8_t _twiSize = 1;
#define set_twiSize(val) _twiSize = val 
#define get_twiSize _twiSize 

uint8_t volatile * _twiReadData;
#define get_twiReadData  _twiReadData
#define set_twiReadData  _twiReadData

volatile uint8_t _twiMTState = avail;
#define twiMTCheck _twiMTState
#define twiMTState(state) _twiMTState = state
uint8_t twiReady(){
    if (_twiMTState == done) return true;
    return false;
}

volatile uint8_t _twiWriteData[10];
void set_twiAllWriteData(uint8_t data[], uint8_t size){
    for (int i = 0; i<size; i++){
        _twiWriteData[i] = data[i];
    }
}

void set_twiWriteData(uint8_t data, uint8_t index){
    if (index < get_twiSize)
        _twiWriteData[index] = data;
}

uint8_t get_twiWriteData(uint8_t index){
    if (index < get_twiSize) return _twiWriteData[index];
}

#define NACK_B4_LAST_TWI_RECEIVE 1

uint8_t _data_size_ = 0;
// Master write process accessed interrupt
uint8_t _twiMasterWrite () {
    switch (get_twiCnt)
    {
        case 1:
            TWCR =  TWI_EN_INT | TWI_SEND_START;
            _data_size_ = get_twiSize;
            break;
        case 2:
            //Check if start completed
            if (TW_STATUS == TW_START){
                TWDR = get_twiSla;
                TWCR = TWI_EN_INT | TWI_SEND;
            }
            else {
                twiMTState(error);
                TWCR = 0;
            }
            break;
        case 3:
            //Check if SLA+W completed
            if (TW_STATUS == TW_MT_SLA_ACK) {
                TWDR = get_twiRegAddr ;
                TWCR = TWI_EN_INT | TWI_SEND;
            } else {
                twiMTState(error);
                TWCR = 0;
            }
            break;

        default:
            if (get_twiCnt > 3) {
                
                //Check if reg addr send completed
                if (TW_STATUS == TW_MT_DATA_ACK) {
                    if (_data_size_ != 0) {
                        TWDR = get_twiWriteData(get_twiSize-_data_size_);
                        TWCR = TWI_EN_INT | TWI_SEND;
                    } else {
                        //Send stop signal when no more data is left.
                        TWCR = TWI_SEND_STOP;
                        twiMTState(done);
                    }
                } 
                else {
                    twiMTState(error);
                    TWCR = 0;
                }
                _data_size_--;
            }
            break;
    }   
}

uint8_t _twiMasterRead () {
    switch (get_twiCnt)
    {
        case 1:
            TWCR =  TWI_EN_INT | TWI_SEND_START;
            _data_size_ = get_twiSize;
            break;
        case 2:
            //Check if start completed
            if (TW_STATUS == TW_START){
                TWDR = get_twiSla;
                TWCR = TWI_EN_INT | TWI_SEND;
            }
            else {
                twiMTState(error);
                TWCR = 0;
            }
            break;
        case 3:
            //Check if SLA+W completed
            if (TW_STATUS == TW_MT_SLA_ACK) {
                TWDR = get_twiRegAddr ;
                TWCR = TWI_EN_INT | TWI_SEND;
            } else {
                twiMTState(error);
                TWCR = 0;
            }
            break;
        case 4:
            if (TW_STATUS == TW_MT_DATA_ACK) {
                //Send, rep-start, enable TWI, enable TWI interrupt
                TWCR = TWI_EN_INT | TWI_SEND_START;
            } else {
                twiMTState(error);
                TWCR = 0;
            }
            break;
        case 5:
            //Check if repeated start completed
            if (TW_STATUS == TW_REP_START){
                TWDR = get_twiSla | 1;
                TWCR = TWI_EN_INT | TWI_SEND;
                _data_size_ = get_twiSize;           
            } else {
                twiMTState(error);
                TWCR = 0;
            }
            break;
        case 6:
            //Check if SLA+R completed
            if (TW_STATUS == TW_MR_SLA_ACK) {
                if ((_data_size_) == 1 && NACK_B4_LAST_TWI_RECEIVE) {
                    TWCR = TWI_EN_INT | TWI_RECV;
                } else TWCR = TWI_EN_INT | TWI_EN_ACK | TWI_RECV;
            } else {
                twiMTState(error);
                TWCR = 0;
            }
            break;
        default:
            if (get_twiCnt > 6) {
                // if (TWDR == 0) onLed();
                // else offLed();
                //Check if reg addr send completed
                if (TW_STATUS == TW_MR_DATA_ACK) {
                    set_twiReadData[get_twiSize-_data_size_] = TWDR;
                    if (_data_size_ > 1) {
                        if (_data_size_ == 2 && NACK_B4_LAST_TWI_RECEIVE){
                            //Stop acknowledging as this is the last byte needed
                            TWCR = TWI_EN_INT | TWI_RECV;
                        } else {
                            // Make sure the data is acknowledged for next data is 
                            // not the last
                            TWCR = TWI_EN_INT | TWI_EN_ACK | TWI_RECV;
                        }
                    }
                    else if (_data_size_ == 1 && !NACK_B4_LAST_TWI_RECEIVE){
                        //Send stop as the last byte has been received
                        TWCR = TWI_SEND_STOP;
                        twiMTState(done);
                    }

                } 
                else if (NACK_B4_LAST_TWI_RECEIVE == true &&
                         TW_STATUS == TW_MR_DATA_NACK && _data_size_ == 1) 
                {
                    set_twiReadData[get_twiSize-_data_size_] = TWDR;
                    //Send stop as the last byte has been received
                    TWCR = TWI_SEND_STOP;
                    twiMTState(done);
                 } else {
                    twiMTState(error);
                    TWCR = 0;                    
                 }
                _data_size_--;
            }
            break;
    }   
}

ISR(TWI_vect){
    // cli(); Done by default
    _twiCnt++;   
    if (get_twiRW == twi_write) _twiMasterWrite();
    else if (get_twiRW == twi_read) _twiMasterRead();
    // sei(); Done by default with reti
}

uint8_t twiMaster(uint8_t sla, uint8_t read_write, uint8_t regAddr, uint8_t data[], uint8_t size) {
    if (_twiMTState == avail) {
        // Initiate twi states
        twiMTState(busy);
        set_twiCnt(1); // Don't change the constant
        set_twiSla(sla);
        set_twiRW(read_write);
        set_twiRegAddr(regAddr);
        set_twiSize(size);
        
        // start transmission
        if (read_write == twi_write) {
            //order of exec. matters
            set_twiAllWriteData(data,size);
            _twiMasterWrite();
        } else {
            get_twiReadData = data;
            _twiMasterRead();
        }
        return ok;
    } 
    else if (twiMTCheck == error) {
        twiMTState(avail);
        return error;
    }
    else if (twiMTCheck == done) {
        twiMTState(avail);
        return done;
    }
    else return busy;
    return error;
}

#endif