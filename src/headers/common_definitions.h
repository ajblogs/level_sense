
#ifndef HEADERS_COMMON_DEFINITIONS_H
#define HEADERS_COMMON_DEFINITIONS_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include "inbuilt_led_driver.h"

int64_t ppow(int64_t n, int p) {
    if (p==0) return 1;
    p--;
    int64_t m = n;
    while(p > 0) { 
        n = m*n;
        p--;
    }
    return n;
}
void intToStr(int64_t i, char cin[]) {
    char c[15];
    int64_t cnt = 0;

    if (i>0) c[cnt] = '+';
    else if (i<0) {
        c[cnt] = '-';
        i *=-1;
    }
    while (1)
    {
        int64_t pl = ppow(10,cnt);
        int64_t ph = ppow(10,(cnt+1));
        int64_t fin = (i%ph - i%pl)/pl;
        if (fin == 0 && i==i%pl) break;
        c[cnt+1] = (char)(0x30|fin);
        cnt++;
    }

    int64_t oin = 0;
    cin[oin] = c[oin];
    cin[cnt+1] = '\0';
    while (cnt > 0) cin[++oin] = c[cnt--];
}

#endif