//Refer ATmega48A/PA/88A/PA/168A/PA/328/P megaAVR ® Data Sheet 
//Section 20 for more information

#ifndef HEADERS_UART_DRIVER_H
#define HEADERS_UART_DRIVER_H

#include "inbuilt_led_driver.h"
#include <avr/interrupt.h>
#include <avr/io.h>

#define true 1
#define false 0

#define ok 0
#define busy 1
#define avail 2
#define error 3
#define done avail

#define UART_BUFFER_SIZE 50
#define RX_DONE (UCSR0A >> 7)
#define TX_DONE ((UCSR0A >> 6) & 1)

//set baud rate to 9600
// For 8 bit character size
// RX complete int. ena., TX complete int. ena., RX ena., TX ena.,
#define initUART() \
    UBRR0 = 103;  \
    UCSR0C = 1<<2 | 1<<1 ; \
    UCSR0B = 1<<7 | 1<<6 | 1<<4 | 1<<3; 

#define uart_send_byte(val) UDR0 = val
#define uart_recv_byte() UDR0


volatile static uint8_t _usartRecvBuffer[UART_BUFFER_SIZE];
volatile static uint8_t _usartRecvIndex = 0;
volatile static uint8_t _usartRecvTailIndex = 0;
uint8_t usartBytesRecvd() {
    if(_usartRecvIndex > _usartRecvTailIndex) {
        return (_usartRecvIndex-_usartRecvTailIndex);
    } else {
        return (_usartRecvTailIndex-_usartRecvIndex);
    }
}

#define incUsartRecvTailIndex() \
    _usartRecvTailIndex++; \
    if (_usartRecvTailIndex>=UART_BUFFER_SIZE) {_usartRecvTailIndex = 0;}

#define incUsartRecvIndex() \
    _usartRecvIndex++; \
    if (_usartRecvIndex>=UART_BUFFER_SIZE) {_usartRecvIndex = 0;} \
    if (_usartRecvIndex == _usartRecvTailIndex) {incUsartRecvTailIndex();}
    // Now a byte is lost if the above if condition is true

ISR(USART_RX_vect){
    _usartRecvBuffer[_usartRecvIndex] = UDR0;
    incUsartRecvIndex();
}

uint8_t usartRead() {
    if (usartBytesRecvd() > 0) {
        uint8_t idx =  _usartRecvTailIndex;
        incUsartRecvTailIndex();
        return _usartRecvBuffer[idx]; 
    } else return false;
}

volatile static char * _usartTmtBuffer;
volatile static uint8_t _usartTmtState = avail;
volatile static uint8_t _usartTmtStateCnt = 0;
volatile static uint8_t _usartTmtSize = 0;
#define set_usartTmtState(val) _usartTmtState = val
#define get_usartTmtState _usartTmtState

#define print(_s_) usartWrite(_s_, sizeof(_s_)/sizeof(char))
#define completePrint(_s_) \
    usartWrite(_s_, sizeof(_s_)/sizeof(char)); \
    while(get_usartTmtState != avail);


uint8_t usartWrite(char c[], uint16_t size) {
    if (get_usartTmtState == avail) {
        _usartTmtSize = size;
        _usartTmtBuffer = c;
        set_usartTmtState(busy);
        _usartTmtStateCnt = 0;
        uart_send_byte(c[0]);
        return true;
    } return busy;
}

void _usartWriteComplete() {
    
    if (get_usartTmtState == busy) {
        _usartTmtStateCnt++; 
        if (_usartTmtStateCnt >= _usartTmtSize || 
            _usartTmtBuffer[_usartTmtStateCnt] == '\0') 
        {
            _usartTmtStateCnt = 0;
            set_usartTmtState(done);
        }
        else uart_send_byte(_usartTmtBuffer[_usartTmtStateCnt]);
    }
}
    
ISR(USART_TX_vect){
    _usartWriteComplete();
}

#endif