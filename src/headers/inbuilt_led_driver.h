#ifndef HEADERS_INBUILD_LED_DRIVER_H
#define HEADERS_INBUILD_LED_DRIVER_H

//Built-in led management
#define initLED() DDRB |= 1<<5
#define onLed() PORTB |= 1<<5
#define offLed() PORTB &= ~(1<<5)
#define toggleLed() PORTB ^= 1<<5

#endif