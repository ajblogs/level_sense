//Refer ATmega48A/PA/88A/PA/168A/PA/328/P megaAVR ® Data Sheet 
//Section 15 for more information

#ifndef HEADERS_TIMER0_DRIVER_H
#define HEADERS_TIMER0_DRIVER_H

#include "inbuilt_led_driver.h"
#include <avr/interrupt.h>
#include <avr/io.h>

// CTC mode
// Clock with Prescale 1024
// OCR0A at 100Hz
//Enable compare match A interrupt
volatile uint8_t every8ms = false;
volatile uint64_t cnt8ms = 0;

#define initTmr0() \
    TCCR0A = 2; \
    TCCR0B = 5; \
    OCR0A = 124; \
    TIMSK0 |= 2;

ISR(TIMER0_COMPA_vect) {
    every8ms = true;
    cnt8ms++;
}

#endif