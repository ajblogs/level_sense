
//Refer ATmega48A/PA/88A/PA/168A/PA/328/P megaAVR ® Data Sheet 
//Section 13 for more information

#ifndef HEADERS_UI_DRIVER_H
#define HEADERS_UI_DRIVER_H

#include <avr/io.h>
#include <util/twi.h>
#include <avr/interrupt.h>
#include "timer0_driver.h"

#define baseDelay 75

#define _Usnapshot 0x21
#define _Usensitivity 0x22
#define _Ureset 0x23
#define _Unone 0x24

volatile uint64_t localRef = 0;
volatile uint8_t userInput = 0;

//Enable interrupt on pin change
#define initUI() \
    DDRB &= ~(1<<1); \
    PORTB |= 1<<1; \
    PCICR = 1; \
    PCMSK0 = 1<<1
    
ISR(PCINT0_vect) {
    //please shift this to main routine
    if (PINB >> 1 & 1) {
        accelIndicatorOff();
        gyroIndicatorOff();
        uint64_t pressP = cnt8ms-localRef;
        if (pressP > (baseDelay*7)) {
            userInput = _Ureset;
        }
        else if (pressP > (baseDelay*3)) {
            userInput = _Usnapshot;
        }
        else if (pressP > 1) {
            userInput = _Usensitivity;
        }
    } else localRef = cnt8ms;       
}
#define userHoldingButton !((PINB>>1) & 1) 
#define userInputReset() userInput = _Unone

#endif