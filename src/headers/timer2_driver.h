//Refer ATmega48A/PA/88A/PA/168A/PA/328/P megaAVR ® Data Sheet
//Section 18 for more information

#ifndef HEADERS_TIMER2_DRIVER_H
#define HEADERS_TIMER2_DRIVER_H

#include "inbuilt_led_driver.h"
#include <avr/interrupt.h>
#include <avr/io.h>

//Toggle OC2A, OC2B on compare match (set on down, clear on up),
//Phase correct PWM mode
//No prescaling
#define initTmr2() \
    DDRB |= 1<<3; \
    DDRD |= 1<<3; \
    TCCR2A = 1<<7 | 1<<5 | 1 ; \
    TCCR2B |= 1; \
    OCR2A = 0; \
    OCR2B = 0

#define accelIndicator8bPWM(val) OCR2B = val   
#define gyroIndicator8bPWM(val) OCR2A = val   
#define accelIndicatorOn() OCR2B = 255   
#define gyroIndicatorOn() OCR2A = 255   
#define accelIndicatorOff() OCR2B = 0   
#define gyroIndicatorOff() OCR2A = 0

#endif