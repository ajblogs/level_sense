#include "headers/twi_driver.h"
#include "headers/uart_driver.h"
#include "headers/timer0_driver.h"
#include "headers/timer2_driver.h"
#include "headers/common_definitions.h"
#include "headers/ui_driver.h"
#include <avr/sleep.h>

//D9 D7 push button

// Timer 2
//D3  -green  D11 - red(Acc) led
#define LSBgyro 16.38
// #define LSBgyro 65.54
#define sample_period 0.024

//If one wants the angle to return to 0 at 360
// #define __360_OFFSET

#define Alimit3 50
#define Alimit2 250
#define Alimit1 500
#define Alimit0 1000


#define Glimit3 1
#define Glimit2 3
#define Glimit1 7
#define Glimit0 12

int main (void) {
        
    //Initiate LED connected to digital pin 13
        initLED();
    //Initiate UART for communication with PC and Debugging purposes
        initUART();
    //Initiate TWI and free the I2C bus
        initTWI();
    //Enable global interrupt
        sei();

    //Welcome
        completePrint("\nInitiating! ");
        char u[15];
        intToStr(123-456789,u);
        completePrint("Cool\n");
        completePrint(u);

    //Initialize required variables
        uint8_t readAccZ[2];
        uint8_t readGyrY[2];
        float Yangle = 0;
        volatile int16_t Az,Gy,Az_once,Gy_once,Az_snapshot = 0;
        uint8_t once = true;
        uint16_t twi_cnt = 0;
        uint16_t print_cnt = 0;
        uint16_t userActivity_cnt = 1;
        uint8_t userSensitivity = 0;
        uint8_t userSnapshot_buffer = 0;
        uint16_t userHoldingButton_upcnt = 0;
        uint16_t userHoldingButton_downcnt = 255;
        
    //Setup the sensor module
        // Activate the sensor module by selecting the clock
        uint8_t writeData[] = {0};
        twiMaster(0b01101000,twi_write,0x6B,writeData,1); 
        while (!twiReady());
        // Configure DLPF 
        writeData[0] = 6;
        twiMaster(0b01101000,twi_write,0x1A,writeData,1); 
        while (!twiReady());
        // Configure Gyro max = 500deg/s = 1, 1000d/s = 2, 2000d/s = 3
        writeData[0] = 0x18;
        twiMaster(0b01101000,twi_write,0x1B,writeData,1); 
        while (!twiReady());

    //Setup the timers
        //Timer 0 triggers the events using every8ms flag and counter value 
        //through cnt8ms.
        initTmr0();
        //Timer 2 is utilized for modulating the indicator
        initTmr2();

    //Setting up push button input
        initUI();


        
    while(1){
        sleep_mode();
        if (every8ms == true) {
            toggleLed();
            every8ms = false;
            if (twiReady()) {
                if(twi_cnt == 0) twiMaster(0b1101000,twi_read,0x3F,readAccZ,2);
                if(twi_cnt == 1) twiMaster(0b1101000,twi_read,0x45,readGyrY,2);
                if(twi_cnt == 2) {
                    twi_cnt = -1;
                    Az = readAccZ[0]<<8 | readAccZ[1];
                    Gy = readGyrY[0]<<8 | readGyrY[1];
                    // if ((Gy < -130 || Gy > -126)) Gy += 128;
                    // else Gy = 0;
                    Gy += 32;
                    Yangle += ((float)Gy/LSBgyro)*sample_period;
                    //Comment the below condition if one wanna measure the 
                    //number of rotations
                    #ifdef __360_OFFSET
                        if(Yangle >= 360) Yangle = Yangle-360;
                        else if(Yangle <= -360) Yangle = Yangle+360;
                    #endif
                }
                twi_cnt++;
            } 
            // else onLed();
        
            if (get_usartTmtState == avail) {
                switch (print_cnt)
                {
                case 0:
                    Az_once = Az;
                    Gy_once = Gy;
                    print("\n\0");                    
                    break;
                case 1:
                    intToStr(Az_once, u);
                    print(u);
                    break;
                case 2:
                    print(",\0");
                    break;
                case 3: 
                    intToStr(Gy_once, u);
                    print(u);
                    break;
                case 4:
                    print(",\0");
                    break;
                case 5:
                    intToStr((int64_t)Yangle, u);
                    print(u);
                    break;
                case 6:
                    print(",\0");
                    break;
                case 7:
                    intToStr((int64_t)Az_snapshot, u);
                    print(u);
                    break;
                default:
                    if(print_cnt >= 100) print_cnt = -1;
                    break;
                }                
                //Controls the delay
                print_cnt++;
            }

            //Act based on the user input
            if (userInput == _Usensitivity) {
                userActivity_cnt++;
                if (userActivity_cnt == 2){
                    userSensitivity++;
                    if (userSensitivity > 3) userSensitivity = 0;
                } 
                else if (userActivity_cnt > 2 && 
                        userActivity_cnt < (baseDelay*2)) {
                    switch (userSensitivity)
                    {
                    case 0:
                        accelIndicator8bPWM(10);
                        gyroIndicator8bPWM(10);
                        break;
                    case 1:
                        accelIndicatorOff();
                        gyroIndicatorOn();
                        break;
                    case 2:
                        accelIndicatorOn();
                        gyroIndicatorOff();
                        break;
                    case 3:
                        accelIndicatorOn();
                        gyroIndicatorOn();
                        break;
                    default:
                        break;
                    } 
                }
                else if (userActivity_cnt > (baseDelay*2)){
                    userActivity_cnt = 1;
                    accelIndicatorOff();
                    gyroIndicatorOff();
                    userInputReset();
                }
            } else if (userInput == _Usnapshot) {
                Az_snapshot = Az;
                Yangle = 0;
                userSnapshot_buffer = 1;
                userInputReset();
            }
            else if (userInput == _Ureset) {
                userSnapshot_buffer = 0;
                userInputReset();
                onLed();
            }
            
            //Check and compare current orientation with the snapshot
            //Display compared results with the indicators if the user is
            //not holding down the button
            if(userSnapshot_buffer && !userHoldingButton && 
               userInput == _Unone) {
                uint8_t withinLimitA = 0,withinLimitG = 0;
                switch (userSensitivity)
                {
                case 0:
                    if(((Az_snapshot+Alimit0) > Az) && 
                        (Az > (Az_snapshot-Alimit0))){
                        withinLimitA = 1;
                        accelIndicatorOff();
                    }
                    if((Glimit0 > Yangle) && (Yangle > -Glimit0)){
                        withinLimitG = 1;
                        gyroIndicatorOff();
                    }
                    break;
                case 1:
                    if(((Az_snapshot+Alimit1) > Az) && 
                        (Az > (Az_snapshot-Alimit1))){
                        withinLimitA = 1;
                        accelIndicatorOff();
                    }
                    if((Glimit1 > Yangle) && (Yangle > -Glimit1)){
                        withinLimitG = 1;
                        gyroIndicatorOff();
                    }
                    break;
                case 2:
                    if(((Az_snapshot+Alimit2) > Az) 
                        && (Az > (Az_snapshot-Alimit2))){
                        withinLimitA = 1;
                        accelIndicatorOff();
                    } 
                    if((Glimit2 > Yangle) && (Yangle > -Glimit2)){
                        withinLimitG = 1;
                        gyroIndicatorOff();
                    }
                    break;
                case 3:
                    if(((Az_snapshot+Alimit3) > Az) && 
                        (Az > (Az_snapshot-Alimit3))){
                        withinLimitA = 1;
                        accelIndicatorOff();
                    } 
                    if((Glimit3 > Yangle) && (Yangle > -Glimit3)){
                        withinLimitG = 1;
                        gyroIndicatorOff();
                    }
                    break;
                default:
                    break;
                }
                if (!withinLimitA) 
                {
                    // accelIndicatorOff();
                    uint16_t diff,div = 0; 
                    if(Az_snapshot>Az) diff = (Az_snapshot - Az);
                    else diff = (Az - Az_snapshot);
                    if(diff>(50*255)) div = 255;
                    else div = diff/50;
                    accelIndicator8bPWM(div);
                }
                if (!withinLimitG)
                {
                    uint8_t diff,div = 0;
                    
                    //No 360 offset
                    #ifndef __360_OFFSET
                        if (Yangle > 255 || Yangle < -255) diff = 255;
                        else if (Yangle<0) diff = -Yangle;
                        else diff = Yangle;
                    #endif
                    
                    //360 offset on
                    #ifdef __360_OFFSET
                        if ((Yangle > 179 && Yangle < 181)||
                            (Yangle < -179 && Yangle > -181)) diff = 255;
                        else if (Yangle > 180) diff = 360 - Yangle;
                        else if (Yangle < -180) diff = 360 + Yangle;
                        else if (Yangle<0) diff = -Yangle;
                        else diff = Yangle;
                    #endif
                    gyroIndicator8bPWM(diff);
                }

            }

            //Give user the visual reference that the button is held down
            else if (userHoldingButton) {
                userActivity_cnt = 1;
                if(userHoldingButton_upcnt < baseDelay){
                    accelIndicator8bPWM (userHoldingButton_upcnt++);
                } 
                else if(userHoldingButton_downcnt < 1){
                    userHoldingButton_downcnt = baseDelay;
                    userHoldingButton_upcnt = 0;
                } else accelIndicator8bPWM (userHoldingButton_downcnt--);                
            } else {
                userHoldingButton_downcnt = baseDelay;
                userHoldingButton_upcnt = 0;
            }

        }
    }
}
